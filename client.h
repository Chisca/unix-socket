#ifndef CLIENT_H
#define CLIENT_H

class Client{

    private:
        int sockfd, portno, n;
        struct sockaddr_in serv_addr;
        struct hostent *server;
        char buffer[256];

    public:
        void start();
        void stop();
        void sendMessage();

};

#endif