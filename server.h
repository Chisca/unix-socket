#ifndef SERVER_H
#define SERVER_H


class Server{

    private:
        int sockfd, newsockfd, portno, clilen;
        char buffer[256];
        struct sockaddr_in serv_addr, cli_addr;
        int  n;

    public:
        void start();
        void stop();
        void sendMessage();

};

#endif