#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include "server.h"
#include <unistd.h>

using namespace std;

void Server :: start(){

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
    if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
    }
    
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = 5001;
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    
    
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }

}

void Server :: sendMessage(){

    newsockfd = accept(sockfd, NULL, NULL);
	
    if (newsockfd < 0) {
        perror("ERROR on accept");
        exit(1);
    }
    
   
    bzero(buffer,256);
    n = read( newsockfd,buffer,255 );
    
    if (n < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }
    
    cout << "Here is the message: " << buffer;
    
    
    n = write(newsockfd,"I got your message",18);
    
    if (n < 0) {
        perror("ERROR writing to socket");
        exit(1);
    }

}

void Server :: stop(){

    close(newsockfd);


}